# FB-LOGIN

## Motivation

Just a simple fb login example

## Installing
1. git clone https://rpukitis@bitbucket.org/rpukitis/fb-login.git
1. npm i
1. npm start
1. http://localhost:3000/
1. login with Facebook test account
1. email open_zmtcuuu_user@tfbnw.net
1. password test1234

![alt text](./out.gif "App animation")