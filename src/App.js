import React, { Component } from 'react'
import { connect } from 'react-redux'
import './App.css'
import { fetchUserLoginStatus } from './store/user'
import Spinner from './components/Spinner'
import Login from './components/Login'
import Card from './components/Card'


const Switch = ({ isLoading, isLogged }) => {
  if (isLoading) {
    return <Spinner />
  }
  if (isLogged) {
    return <Card />
  }
  return <Login />
}


class App extends Component {
  componentDidMount () {
    window.FB.init({
      appId : '870512513130667',
      autoLogAppEvents : true,
      status : true,
      xfbml : true,
      version : 'v2.11',
    })
    this.props.fetchUserLoginStatus()
  }

  render() {
    return (
      <div className="App">
       <Switch {...this.props} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  isLogged: state.user.isLogged,
  isLoading: state.user.isLoading,
})

const mapDispatchToProps = {
  fetchUserLoginStatus
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App)

