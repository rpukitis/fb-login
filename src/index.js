import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import './index.css'
import App from './App'
import configureStore from './store/configureStore'
import registerServiceWorker from './registerServiceWorker'

const Root = () => (
  <Provider store={configureStore()}>
    <App />
  </Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'))
registerServiceWorker()
