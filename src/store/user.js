export const IS_LOADING = 'IS_LOADING'
export const LOGIN_USER = 'LOGIN_USER'
export const LOGOUT_USER = 'LOGOUT_USER'

export const fetchUserLoginStatus = now => (dispatch) => {
  dispatch(isLoading(true))
  window.FB.getLoginStatus((response) => {
    if (response.status !== 'connected') {
      dispatch(isLoading(false))
      return
    }

    window.FB.api('/me', {fields: 'name,email,picture'}, (response) => {
      dispatch(loginUser(response))
    })
  })
}

export const isLoading = (flag) => ({
  type: IS_LOADING,
  flag,
})

export const fetchUserLogin = (now) => (dispatch) => {
  dispatch(isLoading(true))
  window.FB.login((response) => {
    if (response.authResponse) {
      window.FB.api('/me', {fields: 'name,email,picture'}, (response) => {
        dispatch(loginUser(response))
      })
    } else {
      dispatch(logoutUser())
    }
  }, { scope: 'email' })
}

export const loginUser = (payload) => ({
  type: LOGIN_USER,
  payload,
})

export const fetchUserLogout = (now) => (dispatch) => {
  window.FB.getLoginStatus((response) => {
    if (response && response.status === 'connected') {
      window.FB.logout()
      dispatch(logoutUser())
    }
  })
}

export const logoutUser = () => ({
  type: LOGOUT_USER,
})

export default (state = { isLogged: false, isLoading: false }, action) => {
  switch (action.type) {
    case IS_LOADING:
      return {
        isLogged: false,
        isLoading: action.flag,
      }
    case LOGIN_USER:
      return {
        id: action.payload.id,
        name: action.payload.name,
        email: action.payload.email,
        picture: action.payload.picture,
        isLogged: true,
        isLoading: false,
      }
    case LOGOUT_USER:
      return {
        isLogged: false,
        isLoading: false,
      }
    default:
      return state
  }
}
