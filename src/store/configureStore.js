import { combineReducers, createStore, applyMiddleware, compose } from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import user from './user'

const configureStore = () => {
  let composeEnhancers = compose
  const middlewares = [thunk]

  if (process.env.NODE_ENV !== 'production') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
    middlewares.push(createLogger())
  }

  return createStore(
    combineReducers({
      user,
    }),
    composeEnhancers(
      applyMiddleware(...middlewares),
    ),
  )
}

export default configureStore