import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchUserLogin } from '../store/user'
import './Login.css'

class Login extends Component {
  render() {
    const { fetchUserLogin } = this.props
    return (
      <button className="fb-button" onClick={fetchUserLogin}>
        Login with facebook
      </button>
    )
  }
}

export default connect(null, { fetchUserLogin })(Login)
