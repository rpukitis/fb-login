import React from 'react'
import { connect } from 'react-redux'
import { fetchUserLogout  } from '../store/user'
import './Card.css'

const Card = ({name, email, picture, fetchUserLogout}) => (
  <div className="card">
    <div className="card-content">
        <img
          width={`${picture.width}px`}
          height={`${picture.height}px`}
          src={picture.url} alt="profile"
        />
        <div className="card-text card-content-item">
          <span>Name: {name}</span>
          <br />
          <span>Email: {email}</span>
        </div>
    </div>
    <div className="card-footer">
      <button className="card-footer-button" onClick={fetchUserLogout}>Logout</button>
    </div>
  </div>
)

const mapStateToProps = state => ({
  name: state.user.name,
  email: state.user.email,
  picture: state.user.picture.data,
})

const mapDispatchToProps = {
  fetchUserLogout,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Card)